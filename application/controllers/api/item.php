<?php defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class item extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('item_model', 'item');
        $this->load->library(array('ion_auth', 'form_validation'));
    }
	/***
	 * 
	 *
	 * @param  restaurant_id,[start,end]		 
	 * @return list of item by restaurant_id and optional value		
	 * @description 
	 * @author  	ajob
	 * @date 		2014-10-29
	 */
	
    public function list_get()
    {
    	$start = $this->get("start");
		$itemNo = $this->get("itemno");
		
		$restaurant_id = $this->get("restaurant_id");
				
		if( !$restaurant_id )
		{
			$this->response(array( 'error' => 'Restaurent Id Required'), ERROR_STATUS_CODE);
		}
		
        $rows = $this->item->get_list( $restaurant_id , $start , $itemNo );
        $this->response($rows, SUCCESS_STATUS_CODE );
    }

	/***
	 * 
	 *
	 * @param  		 longitude,latitude
	 * @return 		list of item by user logitude , latitude
	 * @description kono dorkar nai :(
	 * @author  	ajob
	 * @date 		2014-10-29
	 */
	function list_by_user_location_get() 
	{
	  $long = $this->get("longitude");
	  $lat = $this->get("latitude");
	  if( !$long || !$lat){
	  	$this->response(array( 'error' => 'Longitude and Latitude is Required'), ERROR_STATUS_CODE);
		  return ;
	  }
	  
	  $this->response( $this->item->get_list_by_location($long,$lat));
	  
	}
	
	/***
	 * 
	 *
	 * @param  		$name ,$start,$itemno
	 * @return 		list of item by item name
	 * @description 
	 * @author  	ajob
	 * @date 		2014-10-29
	 */
	function list_by_name_get() 
	{
		$name = $this->get("name");
		$start = $this->get("start");
		$itemno = $this->get("itemno");
		
		if(!$name )
		{
			$this->response(array( 'error' => 'Item name required'), ERROR_STATUS_CODE);
		  return ;
		}
		
		return $this->response($this->item->get_list_by_name($name,$start,$itemno));
	  
	}
	




/*----------------------
    public function index_get($id)
    {
        $id = (int) $id;

        $row = $this->topic->get($id);

        if (empty($row)) {
            $this->response(array('error_text' => '無此主題'), 404);
        }

        $this->response($row);
    }

    public function index_post()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->response(array('error_text' => '您尚未登入'), 403);
        }

        $id = $this->topic->create(array(
            'title' => $this->post('title'),
            'user_id' => $this->session->userdata('user_id'),
            'description' => $this->post('description'),
            'is_feature' => (bool) $this->post('is_feature')
        ));

        $this->response(array(
            'id' => $id,
            'success_text' => 'ok'
        ));
    }

    public function index_put($id)
    {
        if (!$this->ion_auth->logged_in()) {
            $this->response(array('error_text' => '您尚未登入'), 403);
        }

        $id = (int) $id;

        $row = $this->topic->get($id);

        if (empty($row)) {
            $this->response(array('error_text' => '無此主題'), 404);
        }

        $this->topic->update($id, array(
            'title' => $this->put('title'),
            'description' => $this->put('description'),
            'is_feature' => (bool) $this->put('is_feature')
        ));

        $this->response(array('success_text' => 'ok'));
    }

    public function index_delete($id)
    {
        if (!$this->ion_auth->logged_in()) {
            $this->response(array('error_text' => '您尚未登入'), 403);
        }

        if (!$this->ion_auth->is_admin()) {
            $this->response(array('error_text' => '您並無權限'), 403);
        }

        $id = (int) $id;

        $row = $this->topic->get($id);

        if (empty($row)) {
            $this->response(array('error_text' => '無此主題'), 404);
        }

        $this->topic->delete($id);
        $this->response(array('success_text' => 'ok'));
    } 
 	-------------------------------*/
}
