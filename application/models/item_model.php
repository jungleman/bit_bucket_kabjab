<?php


class item_model extends CI_Model
{
    /***
     * 
     *
     * @param  		start itemNo
     * @return 		array of item
     * @description get list of items form start to end. 
	 * 				return first 10 data if no parameter is passed.
     * @author  	ajob
     * @date 		2014-10-29
     */
    function get_list( $restaurant_id , $start , $itemNo ) 
    {
    	$result ;
    	if( !$start or !$itemNo ){
    		$result = $this->db
    				->limit(10)
    				->from("items")
					->where('restaurant_id',$restaurant_id)
					->get()
					->result_array();
    	}
		
		else {
			//echo $start . $itemNo ; die; 
			$result = $this->db
					->limit($itemNo, $start )
    				->from("items")
					->where('restaurant_id',$restaurant_id)
					->get()
					->result_array();
			
		}
		
		return $result ;
      
    }
	
	/***
	 * 
	 *
	 * @param   longitude, latitude		 
	 * @return 	list of item by user location 
	 * @description given user longitude and latitude. Using google api we will 
	 * 				get a list of nearby resturent . Using this restaurent id 
	 * 				we will return item list
	 * @author  	ajob
	 * @date 		2014-10-29
	 */
	function get_list_by_location( $long , $lat ) 
	{
		
		$restaurant_id_lsit = $this->get_restaurant_from_google($long, $lat);
		return $this->db
			->from('items')
			->where_in("restaurant_id", $restaurant_id_lsit )
			->get()
			->result_array();
	}
	
	/***
	 * 
	 *
	 * @param  		 
	 * @return 		
	 * @description 
	 * @author  	ajob
	 * @date 		2014-10-29
	 */
	function get_restaurant_from_google($long , $lat ) 
	{
	  /**
	   *
	   * @todo       use google api to get nearby restaurant and prepare for query
	   * @date       2014-10-29 *
	   */
	   //moc data
	   //$this->db->from("restaurant")->limit(10)->get();
	   
	   return $resturant_id_nearby = array(1,2);
	  
	}
	/***
	 * 
	 *
	 * @param  		 $name $start $itemno
	 * @return 		item list by item name
	 * @description 
	 * @author  	ajob
	 * @date 		2014-10-29
	 */
	function get_list_by_name($name,$start,$itemno) 
	{
		$result ;
		$name = strtolower($name);
		
    	if( !$start or !$itemno ){
	    	$result = $this->db
					->from("items")
					->like("name",$name,'both')
					->limit(10)
					->get()
					->result_array();
		  
    	}
		
		else {
			//echo $start . $itemNo ; die; 
			$result = $this->db
					->from("items")
					->like("name",$name,'both')
					->limit($itemno,$start)
					->get()
					->result_array();
		}
		
		return $result ;
      
	}
    
}
